import unittest
import threading
event = threading.Event()
filter = "KARGO"
with open('public_html/index.html') as f:
    if filter in f.read():
        print(f"menemukan kata {filter}.")
        berhasil = "false"
    else :
        print(f"Tidak menemukan kata {filter}.")
        berhasil = "true"

class ExpectedFailureTestCase(unittest.TestCase):
    @unittest.expectedFailure
    def test_fail(self):
        self.assertEqual(berhasil, "false")

if __name__ == '__main__':
    unittest.main()